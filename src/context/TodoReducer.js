import {
  SET_ALL_POKEMON,
  SET_OFFSET_PAGE,
  SET_DETAIL_POKEMON,
  SET_POKEMON_NAME,
  SET_LOADING,
} from "./TodoTypes";

const Reducer = (state, { type, payload }) => {
  switch (type) {
    case SET_ALL_POKEMON:
      return {
        ...state,
        allPokemon: payload,
      };
    case SET_OFFSET_PAGE:
      return {
        ...state,
        offsetPage: payload,
      };
    case SET_DETAIL_POKEMON:
      return {
        ...state,
        detailPokemon: payload,
      };
    case SET_POKEMON_NAME:
      return {
        ...state,
        setNameStatus: payload,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    default:
      return state;
  }
};

export default Reducer;
