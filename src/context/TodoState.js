import React, { useReducer } from "react";
import TodoContext from "./TodoContext";
import TodoReducer from "./TodoReducer";
import toast from "react-hot-toast";
import {
  SET_ALL_POKEMON,
  SET_OFFSET_PAGE,
  SET_DETAIL_POKEMON,
  SET_POKEMON_NAME,
  SET_LOADING,
} from "./TodoTypes";

const failedMessage = (name) => {
  toast.error(
    `${
      name === "empty_name" ? "The name cannot be empty" : "Name already used"
    }`
  );
};

const TodoState = ({ children }) => {
  const initialState = {
    allPokemon: [],
    offsetPage: 1,
    isLoading: false,
    detailPokemon: {},
    setNameStatus: false,
  };

  const [state, dispatch] = useReducer(TodoReducer, initialState);

  const getAllPokemon = (offset_page) => {
    dispatch({ type: SET_OFFSET_PAGE, payload: offset_page });
    dispatch({ type: SET_LOADING, payload: true });
    const gqlQuery = `query pokemons($limit: Int, $offset: Int) {
        pokemons(limit: $limit, offset: $offset) {
          count
          next
          previous
          status
          message
          results {
            id
            name
            artwork
          }
        }
      }`;

    const gqlVariables = {
      limit: 12,
      offset: (offset_page - 1) * 12,
    };
    fetch("https://graphql-pokeapi.graphcdn.app/", {
      credentials: "omit",
      body: JSON.stringify({
        query: gqlQuery,
        variables: gqlVariables,
      }),
      method: "POST",
      headers: { "Content-Type": "application/json" },
    })
      .then((res) => res.json())
      .then((res) => {
        dispatch({
          type: SET_ALL_POKEMON,
          payload: res.data.pokemons,
        });
        dispatch({ type: SET_LOADING, payload: false });
      });
  };

  const getPokemonDetail = async (pokemon_name) => {
    dispatch({ type: SET_LOADING, payload: true });
    const gqlQuery = `query pokemon($name: String!) {
        pokemon(name: $name) {
          id
          name
          weight
          height
          moves {
            move {
              name
            }
          }
          types {
            type {
              name
            }
          }
        }
      }`;

    const gqlVariables = {
      name: pokemon_name,
    };
    fetch("https://graphql-pokeapi.graphcdn.app/", {
      credentials: "omit",
      body: JSON.stringify({
        query: gqlQuery,
        variables: gqlVariables,
      }),
      method: "POST",
      headers: { "Content-Type": "application/json" },
    })
      .then((res) => res.json())
      .then((res) => {
        dispatch({
          type: SET_DETAIL_POKEMON,
          payload: res.data.pokemon,
        });
        dispatch({ type: SET_LOADING, payload: false });
      });
  };

  const probCatch = async () => {
    if (Math.random() < 0.5) {
      dispatch({ type: SET_POKEMON_NAME, payload: true });
    }
  };

  const addMyPokemon = (pokemon) => {
    let count_unique = JSON.parse(localStorage.getItem("mypokemons"))
      ? JSON.parse(localStorage.getItem("mypokemons")).filter(
          (obj) => obj.unique_name === pokemon.unique_name
        ).length
      : 0;
    if (pokemon.unique_name === "") {
      failedMessage("empty_name");
    } else {
      if (localStorage) {
        if (count_unique === 0) {
          var pokemons;
          if (!localStorage["mypokemons"]) pokemons = [];
          else pokemons = JSON.parse(localStorage["mypokemons"]);
          if (!(pokemons instanceof Array)) pokemons = [];
          pokemons.push(pokemon);
          localStorage.setItem("mypokemons", JSON.stringify(pokemons));
          window.location.href = `/`;
        } else {
          failedMessage("used_name");
        }
      }
    }
  };

  const releaseMyPokemon = async (pokemon) => {
    var pokemons = JSON.parse(localStorage["mypokemons"]);
    var index = pokemons.findIndex(function (val) {
      return val.unique_name === pokemon;
    });
    if (index !== -1) pokemons.splice(index, 1);
    localStorage.setItem("mypokemons", JSON.stringify(pokemons));
    window.location.href = `/`;
  };

  const {
    allPokemon,
    offsetPage,
    isLoading,
    setNameStatus,
    detailPokemon,
  } = state;

  return (
    <TodoContext.Provider
      value={{
        allPokemon,
        offsetPage,
        isLoading,
        detailPokemon,
        setNameStatus,
        getAllPokemon,
        getPokemonDetail,
        probCatch,
        addMyPokemon,
        releaseMyPokemon,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};

export default TodoState;
