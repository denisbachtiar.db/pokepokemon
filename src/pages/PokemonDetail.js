import React, { useState, useContext, useEffect } from "react";
import TodoContext from "../context/TodoContext";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import PokemonDetailLoading from "../components/pokemon-detail/Loading";
import FormNameDialog from "../components/pokemon-detail/FormNameDialog";

const MovesBox = styled.div`
  border: 1px solid #cecece;
  background-color: white;
  padding: 8px;
  border-radius: 5px;
  max-height: 150px;
  overflow-y: auto;
  margin-top: 5px;
  @media only screen and (max-width: 350px) {
    max-height: 100px;
  }
  p {
    border: 1px solid grey;
    border-radius: 5px;
    padding: 2px;
    margin-right: 5px;
    margin-bottom: 3px;
  }
`;

const BackButton = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
`;

const CatchButton = styled.button`
  margin: 0;
  position: fixed;
  left: 54%;
  bottom: 20px;
  transform: translate(-50%, -50%);
  appearance: auto;
  border: unset;
  padding: 5px 25px;
  border-radius: 10px;
  background-color: #e66a6a;
  color: white;
  box-shadow: 0px 0px 5px 2px rgb(23 23 23 / 12%);
  cursor: pointer;
  img {
    position: absolute;
    left: -30px;
    top: -12px;
  }
`;

const TypesChip = styled.p((props) => ({
  textTransform: "capitalize",
  backgroundColor:
    props.type === "water" || props.type === "flying" || props.type === "ice"
      ? "aliceblue"
      : props.type === "fire"
      ? "orange"
      : props.type === "grass"
      ? "darkseagreen"
      : props.type === "poison"
      ? "fuchsia"
      : props.type === "bug"
      ? "antiquewhite"
      : props.type === "fairy"
      ? "lavenderblush"
      : props.type === "electric"
      ? "yellow"
      : props.type === "dark" || props.type === "ghost"
      ? "lightslategrey"
      : "lightgrey",
  padding: "3px 10px",
  marginRight: "5px",
  borderRadius: "5px",
}));

const PokemonDetail = ({ match }) => {
  const {
    getPokemonDetail,
    detailPokemon,
    probCatch,
    setNameStatus,
    releaseMyPokemon,
    isLoading,
  } = useContext(TodoContext);

  const [catching, setCatching] = useState(false);
  const [modalCatchShow, setModalCatchShow] = useState(false);

  const catchingPokemon = () => {
    setCatching(true);
    setTimeout(() => {
      setCatching(false);
      probCatch();
      setNameStatus
        ? setModalCatchShow(true)
        : toast.error("Failed to catch pokemon");
    }, 2000);
  };

  useEffect(() => {
    getPokemonDetail(match.params.name);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div>
      <Toaster />
      {modalCatchShow ? <FormNameDialog detailPokemon={detailPokemon} /> : ""}
      <Link to={match.params.uniquename !== "0" ? "/" : "/pokemon-world"}>
        <BackButton>
          <img src="/icon/back-arrow.svg" width="35" alt="back-arrow"></img>
        </BackButton>
      </Link>
      {isLoading ? (
        <PokemonDetailLoading />
      ) : (
        <div>
          <div style={{ backgroundColor: "#f7f7f7", padding: "45px 10px" }}>
            {match.params.uniquename !== "0" ? (
              <h3
                className="text-center text-capitalize"
                style={{ color: "grey" }}
              >
                {match.params.uniquename}
              </h3>
            ) : (
              ""
            )}
            <img
              src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${
                detailPokemon && detailPokemon.id
              }.png`}
              style={{ width: "45%", margin: "0 auto", display: "block" }}
              alt="img-pokemon"
            ></img>
          </div>
          <div className="container">
            <div className="d-flex align-center justify-space-between">
              <h3 className="text-uppercase">{detailPokemon.name}</h3>
              <p>
                Height: {detailPokemon.height} | Weight: {detailPokemon.weight}
              </p>
            </div>
            <h6 style={{ marginTop: "20px" }}>Moves</h6>
            <MovesBox className="d-flex align-center flex-row-wrap">
              {detailPokemon.moves &&
                detailPokemon.moves.map((item) => <p>{item.move.name}</p>)}
            </MovesBox>
            <h6 style={{ marginTop: "20px", marginBottom: "5px" }}>Types</h6>
            <div className="d-flex align-center">
              {detailPokemon.types &&
                detailPokemon.types.map((item) => (
                  <TypesChip type={item.type.name}>{item.type.name}</TypesChip>
                ))}
            </div>
          </div>
        </div>
      )}

      {match.params.uniquename !== "0" ? (
        <CatchButton
          type="button"
          onClick={() => releaseMyPokemon(match.params.uniquename)}
        >
          <img
            src="/img/open-pokeball.png"
            width="45"
            alt="img-open-pokeball"
            style={{ left: "-23px", top: "-16px" }}
          ></img>
          Release
        </CatchButton>
      ) : (
        <CatchButton
          type="button"
          disabled={catching}
          onClick={() => catchingPokemon()}
        >
          <img
            src="/img/pokeball.svg"
            style={{
              animation: `${catching ? "spin 2s linear infinite" : "unser"}`,
            }}
            width="45"
            alt="img-pokeball"
          ></img>
          Catch it
        </CatchButton>
      )}
    </div>
  );
};

export default PokemonDetail;
