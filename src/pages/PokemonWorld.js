import React, { useContext, useEffect } from "react";
import TodoContext from "../context/TodoContext";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import PokemonCardLoading from "../components/PokemonCardLoading";
import PokemonCard from "../components/PokemonCard";

const Pagination = styled.div`
  position: sticky;
  bottom: 0;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: rgb(255 221 54 / 42%);
  padding: 5px 10px;
  width: 30%;
  border-radius: 20px;
  display: flex;
  align-items: center;
  button {
    cursor: pointer;
  }
`;

const Button = styled.button`
  border: unset;
  appearance: none;
  background-color: transparent;
`;

const PokemonWorld = () => {
  const { getAllPokemon, offsetPage, allPokemon, isLoading } = useContext(
    TodoContext
  );

  useEffect(() => {
    getAllPokemon(offsetPage);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setOffset = async (mode) => {
    let num = mode === "next" ? (await offsetPage) + 1 : (await offsetPage) - 1;
    getAllPokemon(num);
  };
  return (
    <div className="container" style={{ marginTop: "0px", paddingTop: "10px" }}>
      <div className="d-flex align-center">
        <Link to="/">
          <img src="/icon/back-arrow.svg" width="35" alt="back-arrow"></img>
        </Link>
        <img
          src="/img/mapball.svg"
          style={{ marginLeft: "auto" }}
          width="45"
          alt="img-mapball"
        ></img>
      </div>
      <h3 className="text-black" style={{ marginTop: "10px" }}>
        Pokemon World
      </h3>
      {isLoading ? (
        <PokemonCardLoading level={12} />
      ) : (
        <div
          className="d-flex align-center flex-row-wrap justify-space-between"
          style={{
            marginTop: "15px",
          }}
        >
          {allPokemon.results &&
            allPokemon.results.map((item, i) => (
              <PokemonCard datas={item} key={i} />
            ))}
        </div>
      )}

      <Pagination>
        <Button
          className={!allPokemon.previous ? "disabled-svg" : ""}
          disabled={!allPokemon.previous ? true : false}
          onClick={() => setOffset("prev")}
        >
          <img src="/icon/chevron-left.svg" width="25" alt="back-arrow"></img>
        </Button>
        <h5 style={{ margin: "0 auto", display: "block" }}>
          {offsetPage}/{parseInt(allPokemon.count ? allPokemon.count / 12 : 0)}
        </h5>
        <Button
          className={!allPokemon.next ? "disabled-svg" : ""}
          disabled={!allPokemon.next ? true : false}
          onClick={() => setOffset("next")}
        >
          <img src="/icon/chevron-right.svg" width="25" alt="back-arrow"></img>
        </Button>
      </Pagination>
    </div>
  );
};

export default PokemonWorld;
