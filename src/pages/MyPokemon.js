import { Link } from "react-router-dom";
import styled from "@emotion/styled";
import PokemonCard from "../components/PokemonCard";

const PokemonWorld = styled.div`
  position: absolute;
  bottom: 35px;
  right: 35px;
`;

const HeadTitle = styled.h2`
  position: sticky;
  top: 0;
  width: 100%;
  background-color: white;
  text-align: center;
  padding: 15px 0px;
  color: #e05656;
  text-transform: uppercase;
  box-shadow: 0px 0px 6px 4px rgb(23 23 23 / 6%);
`;

const EmptyPokedex = styled.div`
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
const MyPokemon = () => {
  return (
    <div style={{ height: "100%" }}>
      <HeadTitle>PokePokemon</HeadTitle>
      <div className="container">
        {localStorage.getItem("mypokemons") &&
        JSON.parse(localStorage.getItem("mypokemons")).length > 0 ? (
          <h3 className="text-black" style={{ marginTop: "10px" }}>
            My Pokemon
          </h3>
        ) : (
          ""
        )}
        <div
          className="d-flex align-center flex-row-wrap justify-space-between"
          style={{
            marginTop: "20px",
          }}
        >
          {localStorage.getItem("mypokemons") ? (
            JSON.parse(localStorage.getItem("mypokemons")).length > 0 ? (
              JSON.parse(localStorage.getItem("mypokemons")) &&
              JSON.parse(localStorage.getItem("mypokemons")).map((item) => {
                return <PokemonCard datas={item} mode="mypokemon" />;
              })
            ) : (
              <EmptyPokedex>
                <div>
                  <img
                    src="/img/sad-pikachu.png"
                    width="85"
                    style={{ marginLeft: "30px" }}
                    alt="img-pika"
                  ></img>
                  <p className="text-center">Pokedex is empty</p>
                </div>
              </EmptyPokedex>
            )
          ) : (
            <EmptyPokedex>
              <div>
                <img
                  src="/img/sad-pikachu.png"
                  width="85"
                  style={{ marginLeft: "30px" }}
                  alt="img-pika"
                ></img>
                <p className="text-center">Pokedex is empty</p>
              </div>
            </EmptyPokedex>
          )}
        </div>
      </div>

      <PokemonWorld>
        <Link to="/pokemon-world">
          <img src="/img/mapball.svg" width="45" alt="img-mapball"></img>
        </Link>
      </PokemonWorld>
    </div>
  );
};

export default MyPokemon;
