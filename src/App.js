import "./App.css";

import { BrowserRouter, Route, Switch } from "react-router-dom";

// CONTEXT
import TodoState from "./context/TodoState";

// Pages
import PokemonWorld from "./pages/PokemonWorld";
import MyPokemon from "./pages/MyPokemon";
import PokemonDetail from "./pages/PokemonDetail";

function App() {
  return (
    <TodoState>
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <MyPokemon />
            </Route>
            <Route path="/pokemon-world">
              <PokemonWorld />
            </Route>
            <Route
              path="/detail/:name/:uniquename"
              render={(props) => <PokemonDetail {...props} />}
            />
          </Switch>
        </BrowserRouter>
      </div>
    </TodoState>
  );
}

export default App;
