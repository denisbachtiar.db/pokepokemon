import React from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const Card = styled.div`
  padding: 10px 15px;
  background-color: white;
  font-weight: bold;
  width: 42%;
  box-shadow: 0px 0px 5px 2px rgb(23 23 23 / 6%);
  border-radius: 20px;
  cursor: pointer;
  letter-spacing: 0.5px;
  color: #464646;
  margin-bottom: 15px;
  .owned-text {
    color: #e05656;
    &.not {
      color: lightgray;
    }
  }
  &:hover {
    background-color: #e66a6a;
    color: #ffffff;
    .owned-text {
      color: yellow;
    }
  }
  @media only screen and (max-width: 450px) {
    width: 39%;
  }
`;

const ownedTotal = (id) => {
  return JSON.parse(localStorage.getItem("mypokemons"))
    ? JSON.parse(localStorage.getItem("mypokemons")).filter(
        (obj) => obj.id === id
      ).length || "Not"
    : "Not";
};

const PokemonCard = (props) => {
  return (
    <Card className="text-center" key={props.key}>
      <Link
        to={
          props.mode === "mypokemon"
            ? `detail/${props.datas.name}/${props.datas.unique_name}`
            : `detail/${props.datas.name}/0`
        }
      >
        <img
          src={props.datas.artwork}
          style={{ width: "50%", margin: "0 auto", display: "block" }}
          alt="img-poke"
        ></img>
        <h4 className="text-capitalize">
          {props.mode === "mypokemon"
            ? props.datas.unique_name
            : props.datas.name}
        </h4>
        {props.mode === "mypokemon" ? (
          <h6 className="owned-text text-capitalize">{props.datas.name}</h6>
        ) : (
          <h6
            className={
              ownedTotal(props.datas.id) === "Not"
                ? "owned-text not"
                : "owned-text"
            }
          >
            {ownedTotal(props.datas.id)} Owned
          </h6>
        )}
      </Link>
    </Card>
  );
};

export default PokemonCard;
