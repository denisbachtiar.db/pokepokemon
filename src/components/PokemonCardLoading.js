import React from "react";
import styled from "@emotion/styled";
import Skeleton from "react-loading-skeleton";

const Card = styled.div`
  padding: 10px 15px;
  background-color: white;
  font-weight: bold;
  width: 42%;
  box-shadow: 0px 0px 5px 2px rgb(23 23 23 / 6%);
  border-radius: 20px;
  cursor: pointer;
  letter-spacing: 0.5px;
  color: #464646;
  margin-bottom: 15px;
  .owned-text {
    color: #e05656;
  }
  &:hover {
    background-color: #e66a6a;
    color: #ffffff;
    .owned-text {
      color: yellow;
    }
  }
  @media only screen and (max-width: 450px) {
    width: 39%;
  }
`;

const PokemonCardLoading = (props) => {
  return (
    <div>
      <div
        className="d-flex align-center flex-row-wrap justify-space-between"
        style={{
          marginTop: "15px",
        }}
      >
        {[...Array(props.level)].map(() => (
          <Card className="text-center">
            <Skeleton
              circle={true}
              height={80}
              width={80}
              style={{
                width: "50%",
                margin: "0 auto",
                display: "block",
                marginBottom: "10px",
              }}
            />
            <h4 className="text-capitalize">
              <Skeleton width={90} />
            </h4>
            <h6 className="owned-text">
              <Skeleton width={60} />
            </h6>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default PokemonCardLoading;
