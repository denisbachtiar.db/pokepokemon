import React from "react";
import styled from "@emotion/styled";
import Skeleton from "react-loading-skeleton";

const MovesBox = styled.button`
  border: 1px solid #cecece;
  background-color: white;
  padding: 8px;
  border-radius: 5px;
  max-height: 180px;
  overflow-y: auto;
  margin-top: 5px;
  p {
    border: 1px solid grey;
    border-radius: 5px;
    padding: 2px;
    margin-right: 5px;
    margin-bottom: 3px;
  }
`;

const PokemonDetailLoading = () => {
  return (
    <div>
      <div style={{ backgroundColor: "#f7f7f7", padding: "45px 10px" }}>
        {/* <img
          src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${
            detailPokemon && detailPokemon.id
          }.png`}
          style={{ width: "50%", margin: "0 auto", display: "block" }}
          alt="img-pokemon"
        ></img> */}
        <Skeleton
          circle={true}
          height={170}
          width={170}
          style={{ width: "50%", margin: "0 auto", display: "block" }}
        />
      </div>
      <div className="container">
        <div className="d-flex align-center justify-space-between">
          <h3 className="text-uppercase">
            <Skeleton width={90} />
          </h3>
          <p>
            Height: <Skeleton width={30} /> | Weight: <Skeleton width={30} />
          </p>
        </div>
        <h6 style={{ marginTop: "20px" }}>Moves</h6>
        <MovesBox className="d-flex align-center flex-row-wrap">
          <Skeleton width={50} style={{ marginRight: "5px" }} count={30} />
        </MovesBox>
        <h6 style={{ marginTop: "20px", marginBottom: "5px" }}>Types</h6>
        <div className="d-flex align-center">
          <Skeleton width={50} style={{ marginRight: "5px" }} count={3} />
        </div>
      </div>
    </div>
  );
};

export default PokemonDetailLoading;
