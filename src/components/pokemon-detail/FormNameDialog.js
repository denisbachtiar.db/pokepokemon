import React, { useState, useContext } from "react";
import TodoContext from "../../context/TodoContext";
import styled from "@emotion/styled";

const Dialog = styled.div`
  position: absolute;
  height: 100vh;
  width: 100%;
  background-color: rgb(74 74 74 / 69%);
  z-index: 99;
`;

const CardDialog = styled.div`
  background-color: white;
  padding: 15px;
  width: 60%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 5px;
`;

const Form = styled.input`
  padding: 5px;
  width: 90%;
  margin: unset;
  max-width: 100%;
  display: block;
  margin: 0 auto;
  border: 1px solid #c7c7c7;
  border-radius: 5px;
`;

const ButtonSubmitName = styled.button`
  position: absolute;
  top: 4px;
  right: 15px;
  appearance: auto;
  border: unset;
  padding: 2px 10px;
  border-radius: 5px;
  background-color: #e66a6a;
  color: white;
  font-size: 12px;
  text-transform: capitalize;
  cursor: pointer;
`;

const FormNameDialog = (props) => {
  const { addMyPokemon } = useContext(TodoContext);
  const [name, setPokemonName] = useState("");
  return (
    <Dialog>
      <CardDialog>
        <h5 className="text-center">Congrats you caught this Pokemon</h5>
        <p className="text-center" style={{ marginBottom: "10px" }}>
          Give it a name
        </p>
        <div style={{ position: "relative" }}>
          <Form
            type="text"
            pattern="[a-zA-Z]*"
            name="name"
            onChange={(e) => setPokemonName(e.target.value)}
            value={name}
            required
          ></Form>
          <ButtonSubmitName
            type="submit"
            key="submit"
            htmlType="submit"
            onClick={() => {
              addMyPokemon({
                id: props.detailPokemon.id,
                name: props.detailPokemon.name,
                unique_name: name.toLocaleLowerCase(),
                artwork: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${props.detailPokemon.id}.png`,
              });
            }}
          >
            Done
          </ButtonSubmitName>
        </div>
      </CardDialog>
    </Dialog>
  );
};

export default FormNameDialog;
