import React, { useEffect } from "react";
import styled from "@emotion/styled";

const CardDialog = styled.div`
  background-color: white;
  padding: 10px;
  width: 40%;
  position: absolute;
  top: 10%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 5px;
  box-shadow: 0px 0px 11px 2px rgb(23 23 23 / 12%);
  z-index: 999;
  color: white;
  background-color: #e66a6a;
`;

const Alert = (props) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      console.log("This will run after 1 second!");
    }, 2000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <CardDialog>
      <h5 className="text-center">Failed</h5>
    </CardDialog>
  );
};

export default Alert;
